# Installation

* Update repository list `sudo aptitude update`
* Install git
* run `git clone https://srwalker101@bitbucket.org/srwalker101/raspberrypi-setup.git`
* run `cd raspberrypi-setup`
* Get a few packages
    * `sudo aptitude install -y puppet build-essential ruby1.9.1-dev`
    * `sudo gem install librarian-puppet --no-ri --no-rdoc`
* Install the modules `librarian-puppet install`
* Finally apply the packages `sudo puppet apply manifests/default.pp --modulepath ./modules`

