$packages = ["vim", "tmux", ]

node default {

  exec { 'update':
    command => "aptitude update",
    path => "/usr/bin",
  }

  class { 'nginx':
    require => Exec["update"],
  }

  package { $packages:
    ensure => installed,
    require => Exec["update"],
  }
}
